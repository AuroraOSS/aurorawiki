<div>
	<p align="center">
		<img src="./assets/wiki_banner1020x480.png">
	</p>
	<h1 align="center">Docs for apps by AuroraOSS</h1>

  <p align="center">
      <a href="https://www.gitbook.com/">
        <img src="./assets/powered-by-gitbook.svg">
      </a>
      <img src="https://forthebadge.com/images/badges/built-with-love.svg">
  </p>

  <p align="center">
    <a href="https://t.me/AuroraOSS">
      <img src="https://img.shields.io/badge/Telegram-AuroraOSS-blue?style=flat&logo=telegram"/>
    </a>
    <img alt="License" src="https://img.shields.io/gitlab/license/AuroraOSS/aurorawiki?color=g">
    <img alt="GitLab last commit" src="https://img.shields.io/gitlab/last-commit/AuroraOSS/aurorawiki">
    <a href="https://auroraoss.gitbook.io/auroraoss-wiki/">
      <img alt="Website" src="https://img.shields.io/website?url=https%3A%2F%2Fauroraoss.gitbook.io/auroraoss-wiki/">
    </a>
    <a href="https://www.repostatus.org/#active">
      <img src="https://www.repostatus.org/badges/latest/active.svg" alt="Project Status: Active – The project has reached a stable, usable state and is being actively developed." />
    </a>
    <img alt="Actively Maintained" src="https://img.shields.io/badge/Maintenance%20Level-Actively%20Maintained-default.svg"/>
  </p>
</div>  

## About

This is the root repository for Docs for apps by AuroraOSS hosted at gitbook.io, including:

- [Aurora Store](store),
- [Aurora Droid](droid),
- [App Warden](warden),
- [Aurora Walls](walls). 

[store]:https://gitlab.com/AuroraOSS/AuroraStore
[droid]:https://gitlab.com/AuroraOSS/AuroraDroid
[warden]:https://gitlab.com/AuroraOSS/AppWarden
[walls]:https://gitlab.com/AuroraOSS/AuroraWallpapers

## Contributing

Before doing a PR or commit, read the following documents to make sure your PR or commit is valid:

- [CODE_OF_CONDUCT](CODE_OF_CONDUCT.md)
- [CONTRIBUTING](CONTRIBUTING.md)

If you have any unawnswered questions, please as for help in the Telegram support group chat.

## [License](LICENSE)

<details><summary>View license</summary>
<p>

    MIT License

    Copyright (c) 2023-present AuroraOSS (marchingon12)

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in all
    copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
    SOFTWARE.
</p>
</details>
