# Contributing to this repository 

## Getting started 

Before you begin:
- This site is powered by Gitbook. The main way to contribute is through Gitbook's editor web app.
- Have you read the [code of conduct](CODE_OF_CONDUCT.md)?
- Check out the [existing issues](https://gitlab.com/AuroraOSS/aurorawiki/issues) & see if we [accept contributions](#types-of-contributions-memo) for your type of issue.

### Use the 'Edit page on Gitlab' link

Navigating a new codebase can be challenging, so we're making that a little easier. As you're navigating through  docs.auroraoss.com, you may come across an article that you want to make an update to. You can click on the **Edit page on Gitlab** link at the bottom of that article, which will take you to the file in this repo where you'll make your changes.

Before you make your changes, check to see if an [issue exists](https://gitlab.com/AuroraOSS/aurorawiki/issues/) already for the change you want to make.

### Don't see your issue? Open one!

If you spot something new, open an issue using a [template](https://gitlab.com/AuroraOSS/aurorawiki/-/issues/new). We'll use the issue to have a conversation about the problem you want to fix.

### Ready to make a change? 

These are the possible changes to make:

- Head over to auroraoss.gitbook.io and make your changes, then submit for a review. You will need to contact @AustinHornhead for further information.
- OR fork the repo so that you can make your changes without affecting the original project until you're ready to merge them.

### Make your update:
Make your changes to the file(s) you'd like to update. Here are some tips and tricks for [using the docs codebase](#working-in-the-githubdocs-repository).
  - Are you making changes to the application code? You'll need the latest version of Vuepress andy Vue to run the site locally. See [contributing/development.md](contributing/development.md).
  - Are you contributing to markdown? We use [GitHub Markdown](contributing/content-markup-reference.md).

### Open a pull request
When you're done making changes and you'd like to propose them for review, use the [pull request template](#pull-request-template) to open your PR (pull request).

### Submit your PR & get it reviewed
- Once you submit your PR, others from the Docs community will review it with you. The first thing you're going to want to do is a [self review](#self-review).
- After that, we may have questions, check back on your PR to keep up with the conversation.
- Did you have an issue, like a merge conflict? Check out our [git tutorial](https://lab.gitlab.com/githubtraining/managing-merge-conflicts) on how to resolve merge conflicts and other issues.

### Your PR is merged!
Congratulations! The whole AuroraOSS community thanks you. ✨

Once your PR is merged, you will be proudly listed as a contributor in the [contributor chart](https://gitlab.com/AuroraOSS/aurorawiki/graphs/contributors).

### Keep contributing as you use AuroraOSS Docs

Now that you're a part of the AuroraOSS Docs community, you can keep participating in many ways.

**Learn more about contributing:**

- [Types of contributions 📝](#types-of-contributions)
  - [🪲 Issues](#-issues)
  - [🛠️ Merge requests](#-merge-requests)
  - [📢 Support](#-support)
  - [🌏 Translations](#-translations)
  - [⚖️ Site Policy](#-site-policy)
- [Starting with an issue](#starting-with-an-issue)
  - [Labels](#labels)
- [Opening a pull request](#opening-a-pull-request)
- [Working in the AuroraOSS/aurorawiki repository](#working-in-the-auroraoss/gitbook-repository)
- [Reviewing](#reviewing)
  - [Self review](#self-review)
  - [Merge request template](#merge-request-template)

## Types of contributions 📝
You can contribute to the AuroraOSS Docs content in several ways. This repo is a place to discuss and collaborate on docs.auroraoss.com! Our small, but mighty :muscle: docs team is maintaining this repo, to preserve our bandwidth, off topic conversations will be closed.


### 🪲 Issues
[Issues](https://docs.gitlab.com/en/gitlab/managing-your-work-on-gitlab/about-issues) are used to track tasks that contributors can help with. If an issue has a triage label, we haven't reviewed it yet and you shouldn't begin work on it.

If you've found something in the content or the website that should be updated, search open issues to see if someone else has reported the same thing. If it's something new, open an issue using a [template](https://gitlab.com/AuroraOSS/aurorawiki/issues/new/choose). We'll use the issue to have a conversation about the problem you want to fix.

### 🛠️ Merge requests
A [merge request](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html) is a way to suggest changes in our repository.

When we merge those changes, they should be deployed to the live site within 24 hours. :earth_africa: To learn more about opening a pull request in this repo, see [Opening a pull request](#opening-a-pull-request) below.

### 📢 Support
We are a small team working hard to keep up with the documentation demands of continuously changing products. Unfortunately, we just can't help with support questions in this repository. If you are experiencing a problem with any of our apps, unrelated to our documentation, please [contact AuroraOSS Support directly](https://auroraoss.gitbook.io/auroraoss-docs/contact). Any issues, discussions, or pull requests opened here requesting support will be given information about how to contact AuroraOSS Support, then closed and locked.

### 🌏 Translations

This website is internationalized and available in multiple languages. The source content in this repository is written in English. We integrate with an external localization platform called [Weblate](https://weblate.com) and work with professional translators to localize the English content.

**We do not currently accept contributions for translated content**, but we hope to in the future.

### ⚖️ Site Policy
AuroraOSS' site policies are published [here](https://auroraoss.gitbook.io/auroraoss-docs/references/auroraoss-site-policy)!

If you find a typo in the site policy section, you can open a pull request to fix it.

## Starting with an issue
You can browse existing issues to find something that needs help!

### Labels
Labels can help you find an issue you'd like to help with.
- The `help wanted` label is for problems or updates that anyone in the community can start working on.
- The `good first issue` label is for problems or updates we think are ideal for beginners.
- The `discussion` label is for discussing questions and certain topics of the content on AuroraOSS Docs.

## Opening a pull request
You can use the GitHub user interface :pencil2: for some small changes, like fixing a typo or updating a readme. You can also fork the repo and then clone it locally, to view changes and run your tests on your machine.

## Working in the auroraoss/gitbook repository
Here's some information that might be helpful while working on a Docs PR:

- [Content markup reference](/contributing/content-markup-reference.md) - All of our content is written in Gitbook-flavoured Markdown, with some additional enhancements.

- [Content style guide for AuroraOSS Docs](/contributing/content-style-guide.md) - This guide covers GitHub-specific information about how we style our content and images. It also links to the resources we use for general style guidelines.

## Reviewing
We (usually the docs team, but sometimes AuroraOSS developers, designers, or supporters too!) review every single PR. The purpose of reviews is to create the best content we can for people who use AuroraOSS Apps.

💛 Reviews are always respectful, acknowledging that everyone did the best possible job with the knowledge they had at the time.  
💛 Reviews discuss content, not the person who created it.  
💛 Reviews are constructive and start conversation around feedback.  

### Self review
You should always review your own PR first.

For content changes, make sure that you:
- [ ] Confirm that the changes address every part of the content design plan from your issue (if there are differences, explain them).
- [ ] Review the content for technical accuracy.
- [ ] Review the entire pull request using the [localization checklist](contributing/localization-checklist.md).
- [ ] Copy-edit the changes for grammar, spelling, and adherence to the style guide.
- [ ] Check new or updated Liquid statements to confirm that versioning is correct.
- [ ] Check that all of your changes render correctly in staging. Remember, that lists and tables can be tricky.
- [ ] If there are any failing checks in your PR, troubleshoot them until they're all passing.

### Pull request template
When you open a pull request, you must fill out the "Ready for review" template before we can review your PR. This template helps reviewers understand your changes and the purpose of your pull request.